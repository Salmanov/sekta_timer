'use strict';

var gulp = require('gulp'),
    gp = require('gulp-load-plugins')(),
    uglify = require('gulp-uglify-es').default,
    browserSync = require('browser-sync').create();

gulp.task('serve', function() {
  browserSync.init({
    server: {
      baseDir: "./build"
    },
    ui: {
      port: 3000,
      wienre: {
        port: 9090
      }
    }
  });
  browserSync.watch('build', browserSync.reload)
});

gulp.task('image', function() { // Add the newer pipe to pass through newer images only
  return gulp.src('src/static/img/**')
    .pipe(gp.image())
    .pipe(gulp.dest('build/img'));
});

gulp.task('scripts', function() {
  return gulp.src('src/static/js/main.js')
    .pipe(gp.concat('all.js'))
    .pipe(gp.browserify())
    .pipe(gulp.dest('build/js/'))
});

gulp.task('html', function() {
  return gulp.src('src/index.html')
    .pipe(gulp.dest('build/'))
});

gulp.task('sass', function() {
  return gulp.src('src/static/sass/modules.sass')
    .pipe(gp.sass())
    .pipe(gp.plumber())
    .pipe(gp.sourcemaps.init())
    .pipe(gp.rename({suffix: '.min', prefix : ''}))
    .pipe(gp.autoprefixer(['last 15 versions']))
    .pipe(gp.sourcemaps.write())
    .pipe(gulp.dest('build/css'))
});

gulp.task('watch', function() {
  gulp.watch('src/static/sass/**/*.sass', gulp.series('sass'));
  gulp.watch('src/static/js/**/*.js', gulp.series('scripts'));
  gulp.watch('src/static/img/*', gulp.series('image'));
  gulp.watch('src/**/*.html', gulp.series('html'));
})

gulp.task('default', gulp.series(
  gulp.parallel('sass', 'scripts', 'image', 'html'),
  gulp.parallel('watch', 'serve')
));
