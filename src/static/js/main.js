document.addEventListener("DOMContentLoaded", loaded);

function loaded() {
  const morphProps = {
    'morph4': [
      {
        precision: 10,
        morphIndex: 0
      },
      {
        precision: 10,
        morphIndex: 81
      },
      {
        precision: 10,
        morphIndex: 81
      }
    ],
    'morph7': [
      {
        precision: 2,
        morphIndex: 412
      },
      {
        precision: 1,
        morphIndex: 810
      },
      {
        precision: 2,
        morphIndex: 412
      }
    ],
    'morph8': [
      {
        precision: 3,
        morphIndex: 276
      },
      {
        precision: 1,
        morphIndex: 34
      },
      {
        precision: 1,
        morphIndex: 34
      }
    ]
  }

  function Morph(name, props, morphNumber) {
    this.name = name;
    this.props = props[name];
    this.number = morphNumber;

    let tween = this.makeTween(0, 1, 2);
    let tween2 = this.makeTween(1, 1, 3);
    let tween3 = this.makeTween(2, 1, 1);

    this.run = function () {
      tween.start();
      tween.chain(tween2);
    }
  }

  Morph.prototype.makeTween = function (propsIndex, startPoint, endMorphNumber) {
    const animation = KUTE
      .to( `#morph${this.number}-${startPoint}`, { path: `#morph${this.number}-${endMorphNumber}` }, {
        duration: 3000,
        morphPrecision: this.props[propsIndex].precision,
        morphIndex: this.props[propsIndex].morphIndex
    });

    return animation;
  }

  const morph4 = new Morph('morph4', morphProps, 4);
  const morph7 = new Morph('morph7', morphProps, 7);
  const morph8 = new Morph('morph8', morphProps, 8);

  timer();

  morph4.run();
  morph7.run();
  morph8.run();

  setInterval(() => {
    morph4.run()
    morph7.run()
    morph8.run()
  }, 6000);

  function timer () {
    var countDownDate = new Date("Jul 17, 2019 15:37:25").getTime();

    var x = setInterval(function() {

      var now = new Date().getTime();

      var distance = countDownDate - now;

      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      document.querySelector('.days .big').textContent = days;
      document.querySelector('.hours .big').textContent = hours;
      document.querySelector('.minutes .big').textContent = minutes;
      document.querySelector('.seconds .big').textContent = seconds;
      document.querySelector('.timer__flex').style.opacity = "1"; //исправь это на класс, а то это изварещние

    // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(x);
      }
    }, 1000);
  }
}
